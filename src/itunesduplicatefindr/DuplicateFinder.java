package itunesduplicatefindr;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.UnsupportedAudioFileException;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;

import com.worldsworstsoftware.itunes.ItunesLibrary;
import com.worldsworstsoftware.itunes.ItunesTrack;
import com.worldsworstsoftware.itunes.parser.ItunesLibraryParser;

import javazoom.jl.player.Player;

public class DuplicateFinder {
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");
	private static int deletedEmptyDirectoriesCounter = 0;
	private static ArrayList<String> toDeletelist = new ArrayList<String>();
	private static ArrayList<ItunesTrack> allScannedItunesTracks;
	private static long canceledProgress = 0;

	private static Long lengthOfTask = 1L;
	private static Long current = 0L;
	private static String statMessage = "";
	private static boolean synchronizeSwitchOn;
	private static boolean userCanceled;
	private static long copyTimeNeededInMillis;
	private static double copyAmountInKiloBytes;
	private static boolean done;
	private static boolean compareSongDurations;
	private static boolean extraSensitiveOnArtistName;
	private static String itunesPath = "";

	private static ArrayList<ExtendedItunesTrack> possibleDuplicateAudioFiles;
	private static long startTimeMillis;
	private static ItunesLibrary itunesLibrary;
	private static ArrayList<String> videoFileEndings = new ArrayList<>(Arrays.asList(".m4v", ".mp4", ".avi", ".mkv"));
	private static boolean distinguishRadioEditAndOriginal;
	private static ArrayList<ItunesTrack> allTracks;

	public static void findDuplicates(File directory) {
		resetVariables();
		itunesPath = directory.getAbsolutePath();
		allScannedItunesTracks = new ArrayList<ItunesTrack>();
		possibleDuplicateAudioFiles = new ArrayList<ExtendedItunesTrack>();
		findAllDuplicateItunesTracks();
		done = true;
		current = lengthOfTask;
	}

	public static void resetVariables() {
		statMessage = "";
		current = 0L;
		toDeletelist.clear();
		userCanceled = false;
		canceledProgress = 0;
		done = false;
		copyTimeNeededInMillis = 0;
		copyAmountInKiloBytes = 0;
	}

	// private static ArrayList<File> getFilesArrayListFromDirectoryFile(File file)
	// {
	// String[] currentPathFileNames = file.list();
	// ArrayList<File> resultList = new ArrayList<File>();
	// if (!file.isDirectory() || currentPathFileNames == null) {
	// return null;
	// }
	// for (String currentFilePath : currentPathFileNames) {
	// if (!currentFilePath.toLowerCase().endsWith(".m4a") &&
	// !currentFilePath.toLowerCase().endsWith(".mp3")
	// && currentFilePath.contains(".")) {
	// continue;
	// }
	// resultList.add(new File(file.getAbsolutePath() + FILE_SEPARATOR +
	// currentFilePath));
	// }
	// return resultList;
	// }

	private static void findAllDuplicateItunesTracks() {
		itunesLibrary = ItunesLibraryParser.parseLibrary(itunesPath);
		ArrayList<ItunesTrack> allTracks = getAllTracks();
		if (userCanceled) {
			return;
		}

		// Parallel Processing try
//		int coreNumber = 8;
//		ThreadPoolExecutor pool = new ThreadPoolExecutor(coreNumber, 16, 5, TimeUnit.SECONDS,
//				new ArrayBlockingQueue<Runnable>(10));
//		double allTracksSize = (double) allTracks.size();
//		int tracksPerCore = ((int) (allTracksSize / (double) coreNumber)) + 1;
//		List<List<ItunesTrack>> partitions = new LinkedList<List<ItunesTrack>>();
//		for (int i = 0; i < allTracks.size(); i += tracksPerCore) {
//			partitions.add(allTracks.subList(i, Math.min(i + tracksPerCore, allTracks.size())));
//		}
//
//		for (List<ItunesTrack> partition : partitions) {
//
//			pool.execute(new Runnable() {
//				@Override
//				public void run() {
//
//					for (ItunesTrack currentTrack : partition) {
//						ExtendedItunesTrack currentTrackExtended = new ExtendedItunesTrack(currentTrack);
//						if (userCanceled) {
//							return;
//						}
//						if (mightBeDuplicate(currentTrackExtended)) {
//							possibleDuplicateAudioFiles.add(currentTrackExtended);
//							statMessage = statMessage + "\n" + "Duplicate found (" + possibleDuplicateAudioFiles.size()
//									+ " duplicates so far) - " + (current + 1) + ". of " + lengthOfTask
//									+ " files. Current duplicate: '" + currentTrack.getArtist() + " - "
//									+ currentTrack.getName() + "'";
//						}
//						allScannedItunesTracks.add(currentTrack);
//
//						current++;
//					}
//
//				}
//			});
//
//		}

		for (ItunesTrack currentTrack : allTracks) {
			ExtendedItunesTrack currentTrackExtended = new ExtendedItunesTrack(currentTrack);
			if (userCanceled) {
				return;
			}

			ItunesTrack returnedDuplicateTrack = mightBeDuplicate(currentTrackExtended);
			if (returnedDuplicateTrack != null) {

//				possibleDuplicateAudioFiles.add(currentTrackExtended);
				possibleDuplicateAudioFiles
						.add(getNewerTrackWithOlderDuplicateInExtention(currentTrackExtended, returnedDuplicateTrack));

				statMessage = statMessage + "\n" + "Duplicate found (" + possibleDuplicateAudioFiles.size()
						+ " duplicates so far) - " + (current + 1) + ". of " + lengthOfTask
						+ " files. Current duplicate: '" + currentTrack.getArtist() + " - " + currentTrack.getName()
						+ "'";
			}
			allScannedItunesTracks.add(currentTrack);

			current++;
		}
	}

	private static ExtendedItunesTrack getNewerTrackWithOlderDuplicateInExtention(
			ExtendedItunesTrack currentTrackExtended, ItunesTrack returnedDuplicateTrack) {

		if (returnedDuplicateTrack == null) {
			return currentTrackExtended;
		}

		currentTrackExtended.setOlderDuplicate(returnedDuplicateTrack);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		try {
			Date currentTrackDate = sdf.parse(currentTrackExtended.getDateAdded());
			Date returnedDuplicateTrackDate = sdf.parse(returnedDuplicateTrack.getDateAdded());

			if (currentTrackDate.after(returnedDuplicateTrackDate)) {
				return currentTrackExtended;
			} else {
				ExtendedItunesTrack extendedDuplicateTrack = new ExtendedItunesTrack(returnedDuplicateTrack);
				extendedDuplicateTrack.setLikelyDuplicate(currentTrackExtended.isLikelyDuplicate());
				extendedDuplicateTrack.setOlderDuplicate(currentTrackExtended);
				return extendedDuplicateTrack;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return currentTrackExtended;
	}

	// private static ItunesTrack createSimpleMP3FileFromFile(File file) {
	// MP3File audioFile = null;
	// ID3v24Tag tag = null;
	// Mp4Tag mp4tag = null;
	// try {
	// audioFile = (MP3File) AudioFileIO.read(file);
	// if (audioFile.hasID3v2Tag()) {
	// ID3v24Tag t = audioFile.getID3v2TagAsv24();
	// tag = t;
	// } else {
	// return null;
	// }
	// } catch (CannotReadException ex) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null,
	// ex);
	// } catch (IOException ex) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null,
	// ex);
	// } catch (TagException ex) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null,
	// ex);
	// } catch (ReadOnlyFileException ex) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null,
	// ex);
	// } catch (InvalidAudioFrameException ex) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null,
	// ex);
	// } catch (ClassCastException ex) {
	// // Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null,
	// // ex);
	// try {
	// mp4tag = (Mp4Tag) AudioFileIO.read(file).getTag();
	// } catch (CannotReadException e) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, e);
	// } catch (IOException e) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, e);
	// } catch (TagException e) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, e);
	// } catch (ReadOnlyFileException e) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, e);
	// } catch (InvalidAudioFrameException e) {
	// Logger.getLogger(DuplicateFinder.class.getName()).log(Level.SEVERE, null, e);
	// }
	//
	// }
	// if (tag == null && mp4tag == null) {
	// return null;
	// }
	// String title = "";
	// String artist = "";
	//
	// try {
	// if (tag != null) {
	// title = tag.getFirstField(FieldKey.TITLE).toString().replace("Text=",
	// "").replace("\"", "")
	// .replace(";", "").trim();
	// artist = tag.getFirstField(FieldKey.ARTIST).toString().replace("Text=",
	// "").replace("\"", "")
	// .replace(";", "").trim();
	// } else if (mp4tag != null) {
	// title = mp4tag.getFirst(Mp4FieldKey.TITLE).toString().replace("Text=",
	// "").replace("\"", "")
	// .replace(";", "").trim();
	// artist = mp4tag.getFirst(Mp4FieldKey.ARTIST).toString().replace("Text=",
	// "").replace("\"", "")
	// .replace(";", "").trim();
	// }
	// } catch (Exception e) {
	// System.out.println(e.getMessage());
	// return null;
	// }
	// int durationMillis = 0;
	// if (compareSongDurations) {
	// try {
	// durationMillis = getDurationWithMp3Spi(file);
	// } catch (UnsupportedAudioFileException | IOException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// return new ItunesTrack(title, artist, file.getAbsolutePath(),
	// durationMillis);
	// }

	private static int getDurationWithMp3Spi(File file) throws UnsupportedAudioFileException, IOException {
		int duration = 0;
		try {
			FileInputStream fis = new FileInputStream(file);

			BufferedInputStream bis = new BufferedInputStream(fis);
			Player player = new Player(bis);

			AudioFile audioFile = AudioFileIO.read(file);
			duration = audioFile.getAudioHeader().getTrackLength();

		} catch (Exception e) {
			System.out.print("ERROR " + e);
		}
		return duration;
	}

	private static ItunesTrack mightBeDuplicate(ExtendedItunesTrack itunesTrackToTest) {
		ItunesTrack duplicateTrackForReturn = null;
		if (itunesTrackToTest == null) {
			return null;
		}

		for (ItunesTrack currentScannedItunesTrack : allScannedItunesTracks) {
			String currentArtistLowercase = "";
			String fileToTestArtistLowercase = "";
			String currentTitleLowerCase = "";
			String fileToTestTitleLowercase = "";

			// if (currentScannedItunesTrack.getName().contains("Havana")
			// && itunesTrackToTest.getName().contains("Havana")) {
			// System.out.println("now");
			// }

			currentArtistLowercase = currentScannedItunesTrack.getArtist() == null ? ""
					: currentScannedItunesTrack.getArtist().trim().toLowerCase();
			fileToTestArtistLowercase = itunesTrackToTest.getArtist() == null ? ""
					: itunesTrackToTest.getArtist().trim().toLowerCase();
			currentTitleLowerCase = currentScannedItunesTrack.getName() == null ? ""
					: getTitleWithoutFeaturingPart(currentScannedItunesTrack.getName().trim().toLowerCase());
			fileToTestTitleLowercase = itunesTrackToTest.getName() == null ? ""
					: getTitleWithoutFeaturingPart(itunesTrackToTest.getName().trim().toLowerCase());

			String currentArtistLowercaseNoSeparator = currentArtistLowercase.replace("  ", " ").replace(" & ", "")
					.replace("&", "").replace(" , ", "").replace(", ", "").replace(",", "").replace(" / ", "")
					.replace("/", "").replace(" ; ", "").replace("; ", "").replace(";", "");
			String fileToTestArtistLowercaseNoSeparator = fileToTestArtistLowercase.replace("  ", " ")
					.replace(" & ", "").replace("&", "").replace(" , ", "").replace(", ", "").replace(",", "")
					.replace(" / ", "").replace("/", "").replace(" ; ", "").replace("; ", "").replace(";", "");

			if (currentArtistLowercaseNoSeparator.contains(fileToTestArtistLowercaseNoSeparator)
					|| fileToTestArtistLowercaseNoSeparator.contains(currentArtistLowercaseNoSeparator)
					|| partOfArtistNameInComparedArtist(currentArtistLowercaseNoSeparator,
							fileToTestArtistLowercaseNoSeparator)) {
				boolean oneIsContainingTheOther = false;
				if (currentTitleLowerCase.contains(fileToTestTitleLowercase)
						|| fileToTestTitleLowercase.contains(currentTitleLowerCase)
						|| currentTitleLowerCase.equals(fileToTestTitleLowercase)
						|| fileToTestTitleLowercase.equals(currentTitleLowerCase)) {
					oneIsContainingTheOther = true;
				}
				if (currentTitleLowerCase.equals(fileToTestTitleLowercase)
						|| fileToTestTitleLowercase.equals(currentTitleLowerCase) || oneIsContainingTheOther) {

					if (currentTitleLowerCase.contains("remix") && !fileToTestTitleLowercase.contains("remix")) {
						continue;
					}
					if (currentTitleLowerCase.contains("bootleg") && !fileToTestTitleLowercase.contains("bootleg")) {
						continue;
					}
					if (currentTitleLowerCase.contains("remaster") && !fileToTestTitleLowercase.contains("remaster")) {
						continue;
					}
					boolean oneMightBeRadioEdit = false;
					if (currentTitleLowerCase.contains("edit") && !fileToTestTitleLowercase.contains("edit")) {
						if (currentTitleLowerCase.contains("radio edit")
								&& !fileToTestTitleLowercase.contains("radio edit")) {
							oneMightBeRadioEdit = true;
							if (distinguishRadioEditAndOriginal) {
								continue;
							}
						} else if (!currentTitleLowerCase.contains("radio edit")
								&& !fileToTestTitleLowercase.contains("radio edit")) {
							continue;
						} else {
							continue;
						}
					}
					if (currentTitleLowerCase.contains("cover") && !fileToTestTitleLowercase.contains("cover")) {
						continue;
					}
					if (currentTitleLowerCase.contains("original mix")
							&& !fileToTestTitleLowercase.contains("original mix")) {
					} else if (currentTitleLowerCase.contains("mix") && !fileToTestTitleLowercase.contains("mix")) {
						continue;
					} else if (currentTitleLowerCase.contains("original")
							&& !fileToTestTitleLowercase.contains("original")) {
						continue;
					}
					if (currentTitleLowerCase.contains("extended") && !fileToTestTitleLowercase.contains("extended")) {
						continue;
					}
					if (currentTitleLowerCase.contains("flip") && !fileToTestTitleLowercase.contains("flip")) {
						continue;
					}
					if (currentTitleLowerCase.contains("instrumental")
							&& !fileToTestTitleLowercase.contains("instrumental")) {
						continue;
					}
					if (currentTitleLowerCase.contains("acoustic") && !fileToTestTitleLowercase.contains("acoustic")) {
						continue;
					}
					if (currentTitleLowerCase.contains("live") && !fileToTestTitleLowercase.contains("live")) {
						continue;
					}
					if (currentTitleLowerCase.contains("demo") && !fileToTestTitleLowercase.contains("demo")) {
						continue;
					}
					if (currentTitleLowerCase.contains("unplugged")
							&& !fileToTestTitleLowercase.contains("unplugged")) {
						continue;
					}
					if (currentTitleLowerCase.contains("version") && !fileToTestTitleLowercase.contains("version")) {
						continue;
					}
					if (currentTitleLowerCase.contains("re-crank") && !fileToTestTitleLowercase.contains("re-crank")) {
						continue;
					}
					if (currentTitleLowerCase.contains("re-sauce") && !fileToTestTitleLowercase.contains("re-sauce")) {
						continue;
					}
					if (currentTitleLowerCase.contains("iii") && !fileToTestTitleLowercase.contains("iii")) {
						continue;
					}
					if (currentTitleLowerCase.contains("ii") && !fileToTestTitleLowercase.contains("ii")) {
						continue;
					}

					// same ifs backwards
					if (!currentTitleLowerCase.contains("remix") && fileToTestTitleLowercase.contains("remix")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("bootleg") && fileToTestTitleLowercase.contains("bootleg")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("remaster") && fileToTestTitleLowercase.contains("remaster")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("edit") && fileToTestTitleLowercase.contains("edit")) {
						if (!currentTitleLowerCase.contains("radio edit")
								&& fileToTestTitleLowercase.contains("radio edit")) {
							if (distinguishRadioEditAndOriginal) {
								continue;
							}
							oneMightBeRadioEdit = true;
						} else if (!currentTitleLowerCase.contains("radio edit")
								&& !fileToTestTitleLowercase.contains("radio edit")) {
							continue;
						} else {
							continue;
						}
					}
					if (!currentTitleLowerCase.contains("cover") && fileToTestTitleLowercase.contains("cover")) {
						continue;
					}

					if (!currentTitleLowerCase.contains("original mix")
							&& fileToTestTitleLowercase.contains("original mix")) {
					} else if (!currentTitleLowerCase.contains("mix") && fileToTestTitleLowercase.contains("mix")) {
						continue;
					} else if (!currentTitleLowerCase.contains("original")
							&& fileToTestTitleLowercase.contains("original")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("extended") && fileToTestTitleLowercase.contains("extended")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("flip") && fileToTestTitleLowercase.contains("flip")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("instrumental")
							&& fileToTestTitleLowercase.contains("instrumental")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("acoustic") && fileToTestTitleLowercase.contains("acoustic")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("live") && fileToTestTitleLowercase.contains("live")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("demo") && fileToTestTitleLowercase.contains("demo")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("unplugged")
							&& fileToTestTitleLowercase.contains("unplugged")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("version") && fileToTestTitleLowercase.contains("version")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("re-crank") && fileToTestTitleLowercase.contains("re-crank")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("re-sauce") && fileToTestTitleLowercase.contains("re-sauce")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("iii") && fileToTestTitleLowercase.contains("iii")) {
						continue;
					}
					if (!currentTitleLowerCase.contains("ii") && fileToTestTitleLowercase.contains("ii")) {
						continue;
					}

					// additional ifs
					if (currentTitleLowerCase.contains("intro") && fileToTestTitleLowercase.contains("intro")) {
						continue;
					}

					if (compareSongDurations) {
						if (itunesTrackToTest.getTotalTime() < currentScannedItunesTrack.getTotalTime()) {
							if (itunesTrackToTest.getTotalTime() < 0.95 * currentScannedItunesTrack.getTotalTime()) {
								if (oneMightBeRadioEdit && !distinguishRadioEditAndOriginal) {
								} else {
									continue;
								}
							} else {
								itunesTrackToTest.setLikelyDuplicate(true);
							}
						} else if (currentScannedItunesTrack.getTotalTime() < itunesTrackToTest.getTotalTime()) {
							if (currentScannedItunesTrack.getTotalTime() < 0.95 * itunesTrackToTest.getTotalTime()) {
								if (oneMightBeRadioEdit && !distinguishRadioEditAndOriginal) {
								} else {
									continue;
								}
							} else {
								itunesTrackToTest.setLikelyDuplicate(true);
							}
						} else {
							itunesTrackToTest.setLikelyDuplicate(true);
						}
					}

					if (fileToTestTitleLowercase.length() < currentTitleLowerCase.length()) {
						if ((currentTitleLowerCase.contains("original mix")
								&& !fileToTestTitleLowercase.contains("original mix"))
								|| (!currentTitleLowerCase.contains("original mix")
										&& fileToTestTitleLowercase.contains("original mix"))) {
						} else {
							if (2 * fileToTestTitleLowercase.length() < currentTitleLowerCase.length()) {
								continue;
							}
						}
					} else if (fileToTestTitleLowercase.length() > currentTitleLowerCase.length()) {

						if ((currentTitleLowerCase.contains("original mix")
								&& !fileToTestTitleLowercase.contains("original mix"))
								|| (!currentTitleLowerCase.contains("original mix")
										&& fileToTestTitleLowercase.contains("original mix"))) {
						} else {
							if (2 * currentTitleLowerCase.length() < fileToTestTitleLowercase.length()) {
								continue;
							}
						}
					} else {
						itunesTrackToTest.setLikelyDuplicate(true);
					}

					boolean videoNotVideo = false;
					for (String currVideoFileEnding : videoFileEndings) {
						if (currentScannedItunesTrack.getLocation().endsWith(currVideoFileEnding)
								&& !itunesTrackToTest.getLocation().endsWith(currVideoFileEnding)) {
							videoNotVideo = true;
						}
						if (!currentScannedItunesTrack.getLocation().endsWith(currVideoFileEnding)
								&& itunesTrackToTest.getLocation().endsWith(currVideoFileEnding)) {
							videoNotVideo = true;
						}
					}
					if (videoNotVideo) {
						continue;
					}
					// System.out.println(fileToTestTitleLowercase);
					duplicateTrackForReturn = new ItunesTrack(currentScannedItunesTrack);

					if (currentTitleLowerCase.length() == fileToTestTitleLowercase.length()) {
						itunesTrackToTest.setLikelyDuplicate(true);
						return duplicateTrackForReturn;
					}
					if (currentTitleLowerCase.equals(fileToTestTitleLowercase)) {
						itunesTrackToTest.setLikelyDuplicate(true);
						return duplicateTrackForReturn;
					}
					if (currentTitleLowerCase.length() == fileToTestTitleLowercase.length()) {
						itunesTrackToTest.setLikelyDuplicate(true);
						return duplicateTrackForReturn;
					}
					if (currentTitleLowerCase.equals(fileToTestTitleLowercase)) {
						itunesTrackToTest.setLikelyDuplicate(true);
						return duplicateTrackForReturn;
					}

					return duplicateTrackForReturn;
				}
			}
			duplicateTrackForReturn = null;
		}
		return null;
	}

	private static boolean partOfArtistNameInComparedArtist(String currentArtistLowercaseNoSeparator,
			String fileToTestArtistLowercaseNoSeparator) {

		int nameLengthThreshold = extraSensitiveOnArtistName ? 3 : 4;

		if (currentArtistLowercaseNoSeparator.length() <= nameLengthThreshold
				|| fileToTestArtistLowercaseNoSeparator.length() <= nameLengthThreshold)
			return false;

		if (fileToTestArtistLowercaseNoSeparator
				.contains(currentArtistLowercaseNoSeparator.substring(0, nameLengthThreshold)))
			return true;
		if (currentArtistLowercaseNoSeparator
				.contains(fileToTestArtistLowercaseNoSeparator.substring(0, nameLengthThreshold)))
			return true;
		return false;
	}

	private static boolean titleHasFeaturingPart(String title) {
		String titleLowercase = title.toLowerCase();
		if (titleLowercase.contains("ft. ") || titleLowercase.contains("feat. ")) {
			return true;
		}
		return false;
	}

	private static boolean titleHasFeaturingPartInBrackets(String title) {
		String titleLowercase = title.toLowerCase();
		if (titleLowercase.contains("(ft. ") || titleLowercase.contains("(feat. ")) {
			return true;
		}
		if (titleLowercase.contains("[ft. ") || titleLowercase.contains("[feat. ")) {
			return true;
		}
		return false;
	}

	private static boolean titleHasFeaturingPartInSquareBrackets(String title) {
		String titleLowercase = title.toLowerCase();
		if (titleLowercase.contains("[ft. ") || titleLowercase.contains("[feat. ")) {
			return true;
		}
		return false;
	}

	private static String getTitleWithoutFeaturingPart(String titleInput) {
		String titleLowercase = new String(titleInput).toLowerCase().replace("featuring", "feat.");
		try {
			if (titleHasFeaturingPart(titleLowercase) && !titleHasFeaturingPartInBrackets(titleLowercase)) {
				if (titleLowercase.contains("feat.")) {
					if (!titleLowercase.contains("(") && !titleLowercase.contains("[")) {
						return titleInput.substring(0, titleLowercase.indexOf(" feat. ")).trim();
					} else if (titleLowercase.indexOf(" (") > titleLowercase.indexOf("feat.")) {
						return titleInput.substring(0, titleLowercase.indexOf("feat."))
								+ titleLowercase.substring(titleLowercase.indexOf("("));
						// return titleInput.substring(0, titleLowercase.indexOf(" (")).trim();
					} else if (titleLowercase.indexOf(" [") > titleLowercase.indexOf("feat.")) {
						return titleInput.substring(0, titleLowercase.indexOf(" [")).trim();
					} else {
						return titleInput.substring(0, titleLowercase.indexOf(" feat. ")).trim();
					}
				} else if (titleLowercase.contains("ft. ")) {
					if (!titleLowercase.contains("(") && !titleLowercase.contains("[")) {
						return titleInput.substring(0, titleLowercase.indexOf(" ft. ")).trim();
					} else if (titleLowercase.indexOf(" (") > titleLowercase.indexOf(" ft. ")) {
						return titleInput.substring(0, titleLowercase.indexOf("ft."))
								+ titleLowercase.substring(titleLowercase.indexOf("("));
						// return titleInput.substring(0, titleLowercase.indexOf(" (")).trim();
					} else if (titleLowercase.indexOf(" [") > titleLowercase.indexOf(" ft. ")) {
						return titleInput.substring(0, titleLowercase.indexOf(" [")).trim();
					} else {
						return titleInput.substring(0, titleLowercase.indexOf(" ft.")).trim();
					}
				}
			} else if (titleHasFeaturingPart(titleLowercase) && titleHasFeaturingPartInBrackets(titleLowercase)
					&& !titleHasFeaturingPartInSquareBrackets(titleLowercase)) {
				if (titleLowercase.contains("feat.")) {
					if (countOccurences(titleLowercase, "(") == 1 && !titleLowercase.contains("[")) {
						return titleInput.substring(0, titleLowercase.indexOf(" (feat. ")).trim();
					} else if (countOccurences(titleLowercase, "(") > 1
							&& titleLowercase.indexOf(")") > titleLowercase.indexOf(" (feat.")) {
						return titleInput.substring(0, titleLowercase.indexOf("(feat.")) + titleLowercase
								.substring(titleLowercase.indexOf("feat.")
										+ titleLowercase.substring(titleLowercase.indexOf("feat.")).indexOf(" ("))
								.trim();
					} else if (titleLowercase.indexOf(" [") > titleLowercase.indexOf("feat.")) {
						return titleInput
								.substring(0, titleLowercase.indexOf("feat.")
										+ titleLowercase.substring(titleLowercase.indexOf("feat.")).indexOf(" ["))
								.trim();
					} else {
						return titleInput.substring(0, titleLowercase.indexOf("feat.") - 2).trim();
					}
				} else if (titleLowercase.contains("ft. ")) {
					if (countOccurences(titleLowercase, "(") == 1 && !titleLowercase.contains("[")) {
						return titleInput.substring(0, titleLowercase.indexOf(" (ft. ")).trim();
					} else if (countOccurences(titleLowercase, "(") > 1
							&& titleLowercase.indexOf(")") > titleLowercase.indexOf(" (ft.")) {
						return titleInput.substring(0, titleLowercase.indexOf("(ft."))
								+ titleLowercase
										.substring(titleLowercase.indexOf("ft.")
												+ titleLowercase.substring(titleLowercase.indexOf("ft.")).indexOf(" ("))
										.trim();
					} else if (titleLowercase.indexOf(" [") > titleLowercase.indexOf("ft.")) {
						return titleInput
								.substring(0,
										titleLowercase.indexOf("ft.")
												+ titleLowercase.substring(titleLowercase.indexOf("ft.")).indexOf(" ["))
								.trim();
					} else {
						return titleInput.substring(0, titleLowercase.indexOf("ft.") - 2).trim();
					}
				}
			} else if (titleHasFeaturingPart(titleLowercase) && titleHasFeaturingPartInSquareBrackets(titleLowercase)) {
				if (titleLowercase.contains("feat.")) {
					if (countOccurences(titleLowercase, "(") == 0 && titleLowercase.contains("[")) {
						return titleInput.substring(0, titleLowercase.indexOf(" [feat. ")).trim();
					} else if (countOccurences(titleLowercase, "(") >= 1
							&& titleLowercase.indexOf(" (") > titleLowercase.indexOf(" [feat.")) {
						return titleInput.substring(0, titleLowercase.indexOf("[feat.")).trim() + " "
								+ titleInput.substring(titleLowercase.indexOf("] ") + 1).trim();
					} else if (countOccurences(titleLowercase, "[") > 1
							&& titleLowercase.indexOf("]") > titleLowercase.indexOf("[feat.")) {
						return titleInput.substring(0, titleLowercase.indexOf("[feat.")).trim() + " "
								+ titleInput.substring(titleLowercase.indexOf("]"));
					} else {
						return titleInput.substring(0, titleLowercase.indexOf("feat.") - 2).trim();
					}
				} else if (titleLowercase.contains("ft. ")) {
					if (countOccurences(titleLowercase, "(") == 0 && titleLowercase.contains("[")) {
						return titleInput.substring(0, titleLowercase.indexOf(" [ft. ")).trim();
					} else if (countOccurences(titleLowercase, "(") >= 1
							&& titleLowercase.indexOf(" (") > titleLowercase.indexOf(" [ft.")) {
						return titleInput.substring(0, titleLowercase.indexOf("[ft.")).trim() + " "
								+ titleInput.substring(titleLowercase.indexOf("] ") + 1).trim();
					} else if (countOccurences(titleLowercase, "[") > 1
							&& titleLowercase.indexOf("]") > titleLowercase.indexOf("[ft.")) {
						return titleInput.substring(0, titleLowercase.indexOf("[ft.")).trim() + " "
								+ titleInput.substring(titleLowercase.indexOf("]"));
					} else {
						return titleInput.substring(0, titleLowercase.indexOf("ft.") - 2).trim();
					}
				}
			}

		} catch (Exception ex) {
			// System.out.println(ex.getMessage());
		}
		return titleInput;
	}

	public static ArrayList<ItunesTrack> getAllTracks() {
		itunesLibrary = ItunesLibraryParser.parseLibrary(itunesPath);
		ArrayList<ItunesTrack> allTracks = new ArrayList<>();
		for (Map.Entry<Integer, ItunesTrack> currMapEntry : ((HashMap<Integer, ItunesTrack>) itunesLibrary.getTracks())
				.entrySet()) {
			allTracks.add(currMapEntry.getValue());
		}
		return allTracks;
	}

	/**
	 * This gets the n-th occurence of a substring in a string.
	 * 
	 * @param str
	 * @param substr
	 * @param n
	 * @return
	 */
	public static int ordinalIndexOf(String str, String substr, int n) {
		int pos = str.indexOf(substr);
		while (--n > 0 && pos != -1)
			pos = str.indexOf(substr, pos + 1);
		return pos;
	}

	public static int countOccurences(String str, String substring) {
		int lastIndex = 0;
		int count = 0;

		while (lastIndex != -1) {

			lastIndex = str.indexOf(substring, lastIndex);

			if (lastIndex != -1) {
				count++;
				lastIndex += substring.length();
			}
		}
		return count;
	}

	public static void printAllDuplicateAudioFiles() {
		for (ItunesTrack currFile : possibleDuplicateAudioFiles) {
			System.out.println(currFile.getArtist() + " - " + currFile.getName());
		}
	}

	// Progressbar-Code

	/**
	 * Called from Frontend to start the task.
	 */
	void go(File directory) {
		current = 0L;
		final SwingWorker worker = new SwingWorker() {
			public Object construct() {
				return new ActualTask(directory);
			}
		};
		worker.start();
	}

	/**
	 * Called from Frontend to find out how much work needs to be done.
	 */
	long getLengthOfTask() {
		return lengthOfTask;
	}

	/**
	 * Called from Frontend to find out how much has been done.
	 */
	public static long getCurrent() {
		return current;
	}

	static void setCurrent(long newCurrent) {
		current = newCurrent;
	}

	void stop() {
		statMessage = "Canceled by user.";
		current = lengthOfTask;
	}

	/**
	 * Called from Frontend to find out if the task has completed.
	 */
	boolean done() {
		if (done) {
			return done;
		}
		if (current >= lengthOfTask) {
			return true;
		} else {
			return false;
		}
	}

	String getMessage() {
		return statMessage;
	}

	void clearMessage() {
		statMessage = "";
	}

	/**
	 * The actual long running task. This runs in a SwingWorker thread.
	 */
	class ActualTask {
		ActualTask(File directory) {
			// lengthOfTask = countFilesInDirectory(masterDir);
			startTimeMillis = System.currentTimeMillis();
			findDuplicates(directory);
			if (current > lengthOfTask) {
				current = lengthOfTask;
			}
			// statMessage = "Completed. " + current + " out of " + lengthOfTask + " files
			// processed.";
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
			}
		}
	}

	// Progressbar-Code End

	/**
	 * Returns track number of itunes Library
	 * 
	 * @param itunesMusicLibraryXML the directory to start in
	 * @return the total number of mp3 files
	 */
	public static int getAmountOfTracksInItunesLibrary(File itunesMusicLibraryXML) {
		int count = 0;
		itunesLibrary = ItunesLibraryParser.parseLibrary(itunesMusicLibraryXML.getAbsolutePath());
		count = itunesLibrary.getTracks().size();
		return count;
	}

	public static int countTotalFoldersInDirectory(File directory) {
		int count = 0;
		File[] folderList = directory.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory();
			}
		});

		if (folderList == null || folderList.length < 1) {
			return 0;
		}
		for (File file : folderList) {
			// System.out.println(file.getAbsolutePath());
			count++;
			count += countTotalFoldersInDirectory(file);
		}
		return count;

	}

	public static String getTimeLeftString() {
		if (current == 0) {
			return "";
		}
		Long currentTimeNeededMillis = System.currentTimeMillis() - startTimeMillis;

		Long timeNeededForAllMillis = (long) ((currentTimeNeededMillis.doubleValue() / current.doubleValue())
				* lengthOfTask.doubleValue());
		Long timeLeftSeconds = (timeNeededForAllMillis - currentTimeNeededMillis) / 1000;
		Long timeLeftOnlySeconds = timeLeftSeconds % 60;

		// return "(time left: " + timeLeftSeconds + " secs)";
		Long timeLeftOnlyMinutes = ((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) % 60;
		Long timeLeftOnlyHours = 0L;
		if (((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) > 60) {
			timeLeftOnlyHours = (((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) - timeLeftOnlyMinutes) / 60;
		}
		String timeLeftOnlyHoursString = timeLeftOnlyHours > 9 ? timeLeftOnlyHours.toString() : "0" + timeLeftOnlyHours;
		String timeLeftOnlyMinutesString = timeLeftOnlyMinutes > 9 ? timeLeftOnlyMinutes.toString()
				: "0" + timeLeftOnlyMinutes;
		String timeLeftOnlySecondsString = timeLeftOnlySeconds > 9 ? timeLeftOnlySeconds.toString()
				: "0" + timeLeftOnlySeconds;

		return "(time left: " + timeLeftOnlyHoursString + ":" + timeLeftOnlyMinutesString + ":"
				+ timeLeftOnlySecondsString + ")";
	}

	public static void setSynchronizeSwitchOn(boolean synchronizeSwitchOn) {
		DuplicateFinder.synchronizeSwitchOn = synchronizeSwitchOn;
	}

	public static boolean isSynchronizeSwitchOn() {
		return synchronizeSwitchOn;
	}

	public static void setLengthOfTask(long lengthOfTask) {
		DuplicateFinder.lengthOfTask = lengthOfTask;
	}

	public static void setUserCanceled(boolean userCanceled) {
		DuplicateFinder.userCanceled = userCanceled;
	}

	public static boolean isUserCanceled() {
		return userCanceled;
	}

	public static long getCanceledProgress() {
		return canceledProgress;
	}

	public static void setCanceledProgress(long canceledProgress) {
		DuplicateFinder.canceledProgress = canceledProgress;
	}

	public static double getCurrentAverageCopySpeedInKiloBytesPerSecond() {
		return (double) (copyAmountInKiloBytes / (((double) copyTimeNeededInMillis) / 1000.0));
	}

	public static ArrayList<ExtendedItunesTrack> getPossibleDuplicateAudioFiles() {
		return possibleDuplicateAudioFiles;
	}

	public static void setCompareSongDurations(boolean compareSongDurations) {
		DuplicateFinder.compareSongDurations = compareSongDurations;
	}

	public static int getDeletedEmptyDirectoriesCounter() {
		return deletedEmptyDirectoriesCounter;
	}

	public static void setDeletedEmptyDirectoriesCounter(int deletedEmptyDirectoriesCounter) {
		DuplicateFinder.deletedEmptyDirectoriesCounter = deletedEmptyDirectoriesCounter;
	}

	public static boolean isDistinguishRadioEditAndOriginal() {
		return distinguishRadioEditAndOriginal;
	}

	public static void setDistinguishRadioEditAndOriginal(boolean distinguishRadioEditAndOriginal) {
		DuplicateFinder.distinguishRadioEditAndOriginal = distinguishRadioEditAndOriginal;
	}

	public static boolean isExtraSensitiveOnArtistName() {
		return extraSensitiveOnArtistName;
	}

	public static void setExtraSensitiveOnArtistName(boolean extraSensitiveOnArtistName) {
		DuplicateFinder.extraSensitiveOnArtistName = extraSensitiveOnArtistName;
	}

}

package itunesduplicatefindr;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.worldsworstsoftware.itunes.ItunesTrack;

public class DuplicateFinderPresenter extends JFrame {

	private static final long serialVersionUID = 1L;
	public static final String VERSION = "v1.0.0";
	private static final String NEWEST_VERSION_URL = "https://drive.google.com/file/d/1BJxHp-ZTw8hJdYKxj4Z7oZskMEyU8PPA";
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");
	private static final String ITUNES_LIBRARY_DEFAULT_PATH = System.getProperty("user.home") + FILE_SEPARATOR + "Music"
			+ FILE_SEPARATOR + "iTunes" + FILE_SEPARATOR + "iTunes Music Library.xml";
	public static String NEWEST_VERSION = "";
	private JPanel contentPane;
	private JTextField tfFilePath;
	private JButton btnBrowseDirectory;
	private JButton btnFindDuplicates;
	private JScrollPane scrollPane;
	private String currentPath = ITUNES_LIBRARY_DEFAULT_PATH;
	private String currentPathSlave = System.getProperty("user.home");
	private Timer timer;
	private JProgressBar progressBar;
	private DuplicateFinder duplicateFinder = new DuplicateFinder();
	private JTextArea taskOutput;
	protected long startTimeMillis;
	private JMenuBar menuBar;
	private JMenu mnHelp;
	private JMenuItem miAbout;
	private JMenuItem miCheckForUpdates;
	protected long lastVersionCheckTimeMillis = 0;
	private String averageCopySpeedString = "";
	private static JTable table;
	private JScrollPane scrollPane_1;
	private long progressbarLastUpdatedMillis = 0L;
	private String timeLeftString = "";
	private DuplicateFinderPresenter duplicateFinderPresenter = this;
	private JMenu mnSettings;
	private JCheckBoxMenuItem cbCompareSongDurations;
	private JCheckBoxMenuItem cbDistinguishBetweenRadioEditAndOriginalMix;
	private JCheckBoxMenuItem cbExtraSensitiveRegardingArtist;
	private GridBagConstraints c_1;
	private static int lastCheckedDuplicatesNumber = 0;
	// private Logs logFrame = new Logs("");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException ex) {
					java.util.logging.Logger.getLogger(DuplicateFinderPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				} catch (InstantiationException ex) {
					java.util.logging.Logger.getLogger(DuplicateFinderPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				} catch (IllegalAccessException ex) {
					java.util.logging.Logger.getLogger(DuplicateFinderPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				} catch (javax.swing.UnsupportedLookAndFeelException ex) {
					java.util.logging.Logger.getLogger(DuplicateFinderPresenter.class.getName())
							.log(java.util.logging.Level.SEVERE, null, ex);
				}
				DuplicateFinderPresenter frame = new DuplicateFinderPresenter();
				frame.pack();
				frame.setSize(800, 800);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DuplicateFinderPresenter() {
		// this.setContentPane(new JLabel(new
		// ImageIcon(getClass().getResource("/pictures/tonyc.png"))));
		init();
		getContentPane().setBackground(Color.DARK_GRAY);

		try {
			ImageIcon img = new ImageIcon(getClass().getResource("/itunesduplicatefindr/pictures/icon.png"));
			this.setIconImage(img.getImage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Create a timer.
		timer = new Timer(10, new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				progressBar.setValue((int) DuplicateFinder.getCurrent());
				double averageCopySpeedMBPerSec = (Math.round(
						(DuplicateFinder.getCurrentAverageCopySpeedInKiloBytesPerSecond() / 1000.0) * 100.0) / 100.0);
				if (System.currentTimeMillis() - progressbarLastUpdatedMillis > 1000) {
					if (averageCopySpeedMBPerSec != 0.0) {
						averageCopySpeedString = " (" + averageCopySpeedMBPerSec + " MB/s)";
					}
					timeLeftString = DuplicateFinder.getTimeLeftString();
					progressbarLastUpdatedMillis = System.currentTimeMillis();
				}
				progressBar.setString(String
						.valueOf(Math.round(Double.valueOf(DuplicateFinder.getCurrent())
								/ Double.valueOf(duplicateFinder.getLengthOfTask()) * 100))
						+ " %" + averageCopySpeedString + " " + timeLeftString);
				progressBar.setMaximum((int) duplicateFinder.getLengthOfTask());
				if (!taskOutput.getText().contains(duplicateFinder.getMessage())) {
//					taskOutput.append((DuplicateFinder.getCurrent() == 0 ? "" : "\n") + duplicateFinder.getMessage());
					taskOutput.append(duplicateFinder.getMessage());
					duplicateFinder.clearMessage();
					// logFrame.setLogText(taskOutput.getText() != null ? taskOutput.getText() :
					// "");
				}
				taskOutput.setCaretPosition(taskOutput.getDocument().getLength());

				// Add newly found duplicates to model
				addNewlyFoundDuplicatesToModel();

				if (duplicateFinder.done()) {
					Toolkit.getDefaultToolkit().beep();
					timer.stop();
					Long timeNeeded = System.currentTimeMillis() - startTimeMillis;
					String strTimeNeeded = (timeNeeded > 60000
							? Math.round(((((double) timeNeeded) / 1000.0) / 60.0)) + " min "
							: "") + Math.round(((((double) timeNeeded) / 1000.0) % 60.0)) + " secs";
					String completedOrCanceled = "Completed. ";
					long processedFiles = duplicateFinder.getLengthOfTask();
					if (DuplicateFinder.isUserCanceled()) {
						completedOrCanceled = "Canceled by User. ";
						processedFiles = DuplicateFinder.getCanceledProgress() + 1;
					}
					DefaultTableModel model = (DefaultTableModel) table.getModel();
//					for (ExtendedItunesTrack currTrack : DuplicateFinder.getPossibleDuplicateAudioFiles()) {
//						String path = currTrack.getLocation();
//						if (path != null) {
//							path = path.replace("file://localhost/", "").replace("%20", " ").replace("%21", "!")
//									.replace("%22", "\"").replace("%23", "#").replace("%24", "$").replace("%25", "%")
//									.replace("%26", "&").replace("%27", "'").replace("%2D", "-")
//									.replace("%E2%80%9A", ",").replace("&amp;", "&").replace("&#38;", "&")
//									.replace("%E2%84", "™").replace("%C3%88", "È").replace("%C3%89", "É")
//									.replace("%C3%8A", "Ê").replace("%C3%A8", "è").replace("%C3%A9", "é")
//									.replace("%C3%AA", "ê").replace("%5B", "[").replace("%5D", "]")
//									.replace("%C3%A4", "ä").replace("%C3%84", "Ä").replace("%C3%B6", "ö")
//									.replace("%C3%96", "Ö").replace("%C3%BC", "ü").replace("%C3%9C", "Ü")
//									.replace("%C3%9F", "ß").replace("\"", "");
//						}
//						model.addRow(new Object[] { currTrack.getArtist(), currTrack.getName(),
//								currTrack.isLikelyDuplicate() ? "99%" : "no", path });
//					}
					int duplicatesSize = DuplicateFinder.getPossibleDuplicateAudioFiles().size();
					model.addRow(new Object[] { duplicatesSize + (duplicatesSize == 1 ? " file" : " files") });
					taskOutput.append("\n" + completedOrCanceled + processedFiles + " files processed. Time needed: "
							+ strTimeNeeded);
					DuplicateFinder.printAllDuplicateAudioFiles();
					// logFrame.setLogText(taskOutput.getText() != null ? taskOutput.getText() :
					// "");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					btnFindDuplicates.setEnabled(true);
					btnFindDuplicates.setText("Find Duplicates");
					tfFilePath.setEnabled(true);
					tfFilePath.setEditable(true);
					btnBrowseDirectory.setEnabled(true);
					progressBar.setValue(progressBar.getMinimum());
					progressBar.setString("");
				}
			}
		});

	}

	protected static void addNewlyFoundDuplicatesToModel() {
		ArrayList<ExtendedItunesTrack> newDuplicateAudioFiles = DuplicateFinder.getPossibleDuplicateAudioFiles();
//		System.out.println("CHECK1 " + lastCheckedDuplicatesNumber + " < " + newDuplicateAudioFiles.size());
		if (lastCheckedDuplicatesNumber < newDuplicateAudioFiles.size()) {

			DefaultTableModel model = (DefaultTableModel) table.getModel();
			int iteratorStart = lastCheckedDuplicatesNumber;

			for (int i = iteratorStart; i < newDuplicateAudioFiles.size(); i++) {
				ExtendedItunesTrack currTrack = newDuplicateAudioFiles.get(i);

				String paths = currTrack.getLocation();
				if (paths != null) {
					paths = cleanPathString(paths) + "\n"
							+ cleanPathString(currTrack.getOlderDuplicate().getLocation());
				}
				model.addRow(new Object[] { currTrack.getArtist() + "\n" + currTrack.getOlderDuplicate().getArtist(),
						currTrack.getName() + "\n" + currTrack.getOlderDuplicate().getName(),
						parseMillisToSec(currTrack.getTotalTime()) + "\n"
								+ parseMillisToSec(currTrack.getOlderDuplicate().getTotalTime()),
						currTrack.isLikelyDuplicate() ? "99%" : "no",
						currTrack.getDateAdded() + "\n" + currTrack.getOlderDuplicate().getDateAdded(), paths });
//				System.out.println("--------------- Adding " + currTrack.getName() + " to model.");

			}

		}

		lastCheckedDuplicatesNumber = newDuplicateAudioFiles.size();

	}

	private static String parseMillisToSec(int totalTimeMillis) {
		int timeSec = totalTimeMillis / 1000;
		int minutes = timeSec / 60;
		int seconds = timeSec % 60;

		if (totalTimeMillis % 1000 >= 500) {
			seconds++;
		}

		return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
	}

	private static String cleanPathString(String path) {
		return path.replace("file://localhost/", "").replace("%20", " ").replace("%21", "!").replace("%22", "\"")
				.replace("%23", "#").replace("%24", "$").replace("%25", "%").replace("%26", "&").replace("%27", "'")
				.replace("%2D", "-").replace("%E2%80%9A", ",").replace("&amp;", "&").replace("&#38;", "&")
				.replace("%E2%84", "™").replace("%C3%88", "È").replace("%C3%89", "É").replace("%C3%8A", "Ê")
				.replace("%C3%A8", "è").replace("%C3%A9", "é").replace("%C3%AA", "ê").replace("%5B", "[")
				.replace("%5D", "]").replace("%C3%A4", "ä").replace("%C3%84", "Ä").replace("%C3%B6", "ö")
				.replace("%C3%96", "Ö").replace("%C3%BC", "ü").replace("%C3%9C", "Ü").replace("%C3%9F", "ß")
				.replace("\"", "");
	}

	private void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);

		cbDistinguishBetweenRadioEditAndOriginalMix = new JCheckBoxMenuItem(
				"Differentiate Between Radio Edit and Original Mix");
		cbDistinguishBetweenRadioEditAndOriginalMix.setSelected(true);
		mnSettings.add(cbDistinguishBetweenRadioEditAndOriginalMix);

		cbCompareSongDurations = new JCheckBoxMenuItem("Only Songs with Similar Lenght");
		cbCompareSongDurations.setSelected(true);
		mnSettings.add(cbCompareSongDurations);

		cbExtraSensitiveRegardingArtist = new JCheckBoxMenuItem("Extra Sensitive Search (Likely More False Positives)");
		cbExtraSensitiveRegardingArtist.setSelected(false);
		mnSettings.add(cbExtraSensitiveRegardingArtist);

		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		miAbout = new JMenuItem("About");
		miAbout.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					About aboutFrame = new About();
					aboutFrame.setLocationRelativeTo(duplicateFinderPresenter);
					aboutFrame.setVisible(true);
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});

		miCheckForUpdates = new JMenuItem("Check for Updates");
		miCheckForUpdates.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (NEWEST_VERSION.length() < 2
						|| ((System.currentTimeMillis() - lastVersionCheckTimeMillis) / 1000) > 60) {
					NEWEST_VERSION = getNewestVersionNumber();
					lastVersionCheckTimeMillis = System.currentTimeMillis();
				}
				// miUpdateAvailable.setVisible(!NEWEST_VERSION.equals(VERSION));
				if (NEWEST_VERSION.equals(VERSION)) {
					JOptionPane.showMessageDialog(duplicateFinderPresenter, "This version is up to date.");
				} else {
					Object[] options = { "Yes, please!", "Maybe later" };
					int userChoice = JOptionPane.showOptionDialog(duplicateFinderPresenter,
							"New version available! (" + NEWEST_VERSION + ").\nDownload it now?", "Updates",
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
					if (userChoice == JOptionPane.YES_OPTION) {
						try {
							Desktop.getDesktop().browse(new URI(
									"https://drive.google.com/uc?id=1BJxHp-ZTw8hJdYKxj4Z7oZskMEyU8PPA&export=download"));
						} catch (IOException | URISyntaxException ex) {
							System.out.println("Could not open update page.");
						}
					}
				}

			}
		});

		mnHelp.add(miCheckForUpdates);
		mnHelp.add(miAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		// contentPane.setLayout(new GridLayout(11, 4, 2, 2));
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0 };
		gbl_contentPane.columnWeights = new double[] { 1.0 };
		contentPane.setLayout(gbl_contentPane);
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(0, 0, 5, 0);
		int paddingTop = 8;
		int width = 150;

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = width;
		c.gridx = 0;
		c.gridy = 0;
		// FilePath Master
		JLabel lblFilePath = new JLabel("iTunes Music Library.xml Path");
		lblFilePath.setForeground(Color.WHITE);
		lblFilePath.setFont(new Font("Tahoma", Font.BOLD, 18));
		contentPane.add(lblFilePath, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 8;
		c.gridx = 0;
		c.gridy = 1;
		tfFilePath = new JTextField();
		tfFilePath.setText(ITUNES_LIBRARY_DEFAULT_PATH);
		tfFilePath.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					browseMasterPath();
				}
			}
		});
		contentPane.add(tfFilePath, c);
		tfFilePath.getDocument().addDocumentListener(new DocumentListener() {
			void changeMasterDirectory() {
				if ((new File(tfFilePath.getText())).exists()) {
					currentPath = tfFilePath.getText().trim();
				} else {
					currentPath = "";
				}
				System.out.println("currentPathMaster=" + currentPath);
			}

			public void changedUpdate(DocumentEvent e) {
				changeMasterDirectory();
			}

			public void removeUpdate(DocumentEvent e) {
				changeMasterDirectory();
			}

			public void insertUpdate(DocumentEvent e) {
				changeMasterDirectory();
			}
		});

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 5, 0);
		c.ipadx = width;
		c.ipady = 15;
		c.gridx = 0;
		c.gridy = 2;
		btnBrowseDirectory = new JButton();
		btnBrowseDirectory.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBrowseDirectory.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				browseMasterPath();
			}
		});

		btnBrowseDirectory.setText("Browse for Directory");
		contentPane.add(btnBrowseDirectory, c);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.getViewport().setBackground(Color.DARK_GRAY);
		c_1 = new GridBagConstraints();
		c_1.insets = new Insets(0, 0, 5, 0);
		c_1.fill = GridBagConstraints.HORIZONTAL;
		c_1.gridx = 0;
		c_1.gridy = 4;
		c_1.ipady = 400;
		contentPane.add(scrollPane_1, c_1);

		table = new JTable();
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		table.setForeground(Color.WHITE);
		table.setBackground(Color.DARK_GRAY);
		table.setFont(new Font("Tahoma", Font.BOLD, 11));
		table.setRowHeight(25);
		table.setIntercellSpacing(new Dimension(20, 1));
		table.setGridColor(new Color(40, 40, 40));
		table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
		scrollPane_1.setViewportView(table);
		table.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Artist Name", "Song Title", "Length", "Sure", "Date Added", "Path" }) {
			Class[] columnTypes = new Class[] { String.class, String.class, String.class, String.class, String.class,
					String.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
		table.getColumnModel().getColumn(0).setPreferredWidth(150);
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(1).setPreferredWidth(220);
		table.getColumnModel().getColumn(1).setMinWidth(50);
		table.getColumnModel().getColumn(2).setMinWidth(50);
		table.getColumnModel().getColumn(2).setPreferredWidth(60);
		table.getColumnModel().getColumn(2).setMaxWidth(70);
		table.getColumnModel().getColumn(3).setMinWidth(50);
		table.getColumnModel().getColumn(3).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setMaxWidth(50);
		table.getColumnModel().getColumn(4).setMinWidth(50);
		table.getColumnModel().getColumn(4).setPreferredWidth(155);
		table.getColumnModel().getColumn(4).setMaxWidth(160);
		table.getColumnModel().getColumn(5).setPreferredWidth(100);
		table.getColumnModel().getColumn(5).setMinWidth(50);
		class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {

			public MultiLineCellRenderer() {
//				setLineWrap(true);
				setWrapStyleWord(true);
				setOpaque(true);
			}

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				if (isSelected) {
					setForeground(table.getSelectionForeground());
					setBackground(table.getSelectionBackground());
				} else {
					setForeground(table.getForeground());
					setBackground(table.getBackground());
				}
				setFont(table.getFont());
				if (hasFocus) {
					setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
					if (table.isCellEditable(row, column)) {
						setForeground(UIManager.getColor("Table.focusCellForeground"));
						setBackground(UIManager.getColor("Table.focusCellBackground"));
					}
				} else {
					setBorder(new EmptyBorder(1, 2, 1, 2));
				}
				setText((value == null) ? "" : value.toString());
				return this;
			}
		}
		MultiLineCellRenderer renderer = new MultiLineCellRenderer();
		table.setRowHeight((int) (table.getRowHeight() * 1.4));
		table.setDefaultRenderer(String.class, renderer);
		table.setRowSorter(sorter);

		List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
		// sortKeys.add(new RowSorter.SortKey(3, SortOrder.ASCENDING));
		// sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
		sorter.setSortKeys(sortKeys);

		// c = new GridBagConstraints();
		// c.fill = GridBagConstraints.HORIZONTAL;
		// c.insets = new Insets(24, 0, 5, 0);
		// c.ipadx = width;
		// c.gridx = 0;
		// c.gridy = 5;
		// JSeparator separator = new JSeparator();
		// separator.setBackground(SystemColor.scrollbar);
		// contentPane.add(separator, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 0, 0);
		c.ipadx = width;
		c.ipady = 10;
		c.gridx = 0;
		c.gridy = 5;
		btnFindDuplicates = new JButton();
		// btnFindDuplicates
		// .setIcon(new
		// ImageIcon(DuplicateFinderPresenter.class.getResource("/pictures/icon_small.png")));
		btnFindDuplicates.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnFindDuplicates.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!btnFindDuplicates.isEnabled()) {
					return;
				}
				if (DuplicateFinder.getCurrent() != 0
						&& DuplicateFinder.getCurrent() < duplicateFinder.getLengthOfTask()
						&& DuplicateFinder.isSynchronizeSwitchOn() && btnFindDuplicates.getText().equals("Cancel")) {
					int choice = JOptionPane.showConfirmDialog(duplicateFinderPresenter,
							"Are you sure you want to cancel now?");
					if (choice == JOptionPane.YES_OPTION) {
						DuplicateFinder.setCanceledProgress(DuplicateFinder.getCurrent());
						DuplicateFinder.setCurrent(duplicateFinder.getLengthOfTask());
						btnFindDuplicates.setText("Find Duplicates");
						DuplicateFinder.setUserCanceled(true);
					}
					return;
				}

				if (!new File(currentPath).exists() || tfFilePath.getText() == null
						|| tfFilePath.getText().trim().equals("")) {
					JOptionPane.showMessageDialog(duplicateFinderPresenter, "Please enter a valid Directory!");
					return;
				}

				int choice = JOptionPane.showConfirmDialog(duplicateFinderPresenter,
						"Are you sure?\nThis may take a while.");
				if (choice == JOptionPane.YES_OPTION) {
					// DirectoryManager.synchronizeMasterToSlave(new File(currentPathMaster), new
					// File(currentPathSlave));
//					table.setModel(new DefaultTableModel(new Object[][] {},
//							new String[] { "Artist Name", "Song Title", "Sure", "Path" }) {
//						Class[] columnTypes = new Class[] { String.class, String.class, String.class, String.class };
//
//						public Class getColumnClass(int columnIndex) {
//							return columnTypes[columnIndex];
//						}
//
//						boolean[] columnEditables = new boolean[] { true, true, false, false };
//
//						public boolean isCellEditable(int row, int column) {
//							return columnEditables[column];
//						}
//					});

					((DefaultTableModel) table.getModel()).setRowCount(0);
					TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
					table.setRowSorter(sorter);

					List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
					// sortKeys.add(new RowSorter.SortKey(4, SortOrder.ASCENDING));
					// sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
					sorter.setSortKeys(sortKeys);

					DuplicateFinder.resetVariables();
					DuplicateFinder.setSynchronizeSwitchOn(true);
					taskOutput.setText("");
					tfFilePath.setEnabled(false);
					tfFilePath.setEditable(false);
					btnBrowseDirectory.setEnabled(false);
					btnFindDuplicates.setText("Cancel");
					progressBar.setString("Calculating...");
					startTimeMillis = System.currentTimeMillis();
					int lengthOfTask = DuplicateFinder.getAmountOfTracksInItunesLibrary(new File(currentPath));
					progressBar.setMaximum(lengthOfTask);
					DuplicateFinder.setLengthOfTask(lengthOfTask);
					DuplicateFinder.setCompareSongDurations(cbCompareSongDurations.isSelected());
					DuplicateFinder.setDistinguishRadioEditAndOriginal(
							cbDistinguishBetweenRadioEditAndOriginalMix.isSelected());
					DuplicateFinder.setExtraSensitiveOnArtistName(cbExtraSensitiveRegardingArtist.isSelected());
					duplicateFinder.go(new File(currentPath));
					timer.start();
				}
			}
		});

		btnFindDuplicates.setText("Find Duplicates");
		contentPane.add(btnFindDuplicates, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(8, 0, 0, 0);
		c.ipadx = 1;
		c.gridx = 0;
		c.ipady = 10;
		c.gridy = 6;
		progressBar = new JProgressBar(0, ((int) duplicateFinder.getLengthOfTask()));
		progressBar.setStringPainted(true);
		progressBar.setString("");
		contentPane.add(progressBar, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.ipadx = width;
		c.gridx = 0;
		c.gridy = 7;
		c.gridheight = 2;
		c.insets = new Insets(8, 0, 0, 0);
		scrollPane = new JScrollPane();
		taskOutput = new JTextArea();
		taskOutput.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					// logFrame.setLogText(taskOutput.getText() != null ? taskOutput.getText() :
					// "");
					// logFrame.setVisible(true);
				}
			}
		});
		taskOutput.setWrapStyleWord(true);

		scrollPane.setViewportView(taskOutput);
		taskOutput.setFont(new Font("Monospaced", Font.PLAIN, 12));
		taskOutput.setLineWrap(true);
		taskOutput.setEditable(false);
		taskOutput.setMargin(new Insets(5, 5, 5, 5));
		contentPane.add(scrollPane, c);

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("iTunes Duplicate FindR by TONYC");
		// pack();
	}

	protected void browseMasterPath() {
		if (!btnBrowseDirectory.isEnabled()) {
			return;
		}
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(currentPath));
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setVisible(true);
		JButton open = new JButton();
		while (fileChooser.isVisible()) {
			int option = fileChooser.showOpenDialog(open);
			if (option == JFileChooser.APPROVE_OPTION || option == JFileChooser.CANCEL_OPTION) {
				fileChooser.setVisible(false);
			}
		}
		if (fileChooser.getSelectedFile() != null) {
			tfFilePath.setText(fileChooser.getSelectedFile().getAbsolutePath());
			currentPath = fileChooser.getSelectedFile().getAbsolutePath();
		}
	}

	private static String getNewestVersionNumber() {
		StringBuffer htmlCode = new StringBuffer("");
		HttpURLConnection connection;
		try {
			connection = (HttpURLConnection) new URL(NEWEST_VERSION_URL).openConnection();
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.setRequestProperty("Accept-Language", "de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7");
			connection.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,image/jpg,image/jpeg,*/*;q=0.8");
			connection.connect();
			BufferedReader in = new BufferedReader(
					new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

			String inputLine;
			htmlCode = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				htmlCode.append(inputLine);
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String versionString = "";

		int pointer = htmlCode.indexOf("content=\"DirectorySynchronizR_");
		int firstPointer = pointer;
		String pointerValue = "";
		while (!pointerValue.toLowerCase().equals("v") && pointer < (firstPointer + 1000)) {
			pointer++;
			pointerValue = htmlCode.substring(pointer, pointer + 1);
		}
		int versionBeginIndex = pointer;
		while (!pointerValue.toLowerCase().equals(".jar") && pointer < (firstPointer + 1000)) {
			pointer++;
			pointerValue = htmlCode.substring(pointer, pointer + 4);
		}
		int versionEndIndex = pointer;
		versionString = htmlCode.substring(versionBeginIndex, versionEndIndex);
		return versionString;
	}

}

package itunesduplicatefindr;

import com.worldsworstsoftware.itunes.ItunesTrack;

public class ExtendedItunesTrack extends ItunesTrack {
	// private ItunesTrack itunesTrack;
	private boolean isLikelyDuplicate;
	private ItunesTrack olderDuplicate;

	// public ExtendedItunesTrack(ItunesTrack itunesTrack) {
	// this.itunesTrack = itunesTrack;
	// }

	// public ExtendedItunesTrack(ItunesTrack itunesTrack, boolean
	// isLikelyDuplicate) {
	// this.itunesTrack = itunesTrack;
	// this.isLikelyDuplicate = isLikelyDuplicate;
	// }

	public ExtendedItunesTrack() {
		super();
	}

	public ExtendedItunesTrack(ItunesTrack track) {
		super(track);
	}

	public ExtendedItunesTrack(boolean isLikelyDuplicate) {
		super();
		this.isLikelyDuplicate = isLikelyDuplicate;
	}

	// public ItunesTrack getItunesTrack() {
	// return itunesTrack;
	// }
	//
	// public void setItunesTrack(ItunesTrack itunesTrack) {
	// this.itunesTrack = itunesTrack;
	// }

	public boolean isLikelyDuplicate() {
		return isLikelyDuplicate;
	}

	public void setLikelyDuplicate(boolean isLikelyDuplicate) {
		this.isLikelyDuplicate = isLikelyDuplicate;
	}

	public ItunesTrack getOlderDuplicate() {
		return olderDuplicate;
	}

	public void setOlderDuplicate(ItunesTrack olderDuplicate) {
		this.olderDuplicate = olderDuplicate;
	}

}
